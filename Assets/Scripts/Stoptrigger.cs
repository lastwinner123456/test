using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stoptrigger : MonoBehaviour
{
    [SerializeField] List<GameObject> enemies;
    GameObject player;
    [SerializeField] GameObject imageComplete;
    [SerializeField]bool islastScene;
    [SerializeField] bool isFirstWaypoint;
    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    private void Update()
    {
        
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i] == null)
                {
                    enemies.Remove(enemies[i]);
                }
            }
            if (enemies.Count == 0)
            {

                if (islastScene)
                {
                    player.GetComponent<PlayerController>().isStop = true;
                    imageComplete.SetActive(true);
                }
                else
                {
                    player.GetComponent<PlayerController>().isStop = false;
                    Destroy(gameObject);
                }
            }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().isStop = true;
        }
    }
}
