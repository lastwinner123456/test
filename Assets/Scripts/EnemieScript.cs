using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemieScript : MonoBehaviour
{
    Animator anim;
    [SerializeField] int hp;
    float maxHp;
    [SerializeField] GameObject healthBar;
    Image image;
  //  [SerializeField] EnemyManager enemyManager;
    void Start()
    {
        image = healthBar.GetComponent<Image>();
        anim = GetComponent<Animator>();
        maxHp = hp;
        image.fillAmount = hp / maxHp;
    }

    public void MakeDamage(int damage)
    {
        hp = hp - damage;
        anim.SetTrigger("Hit");
        image.fillAmount = hp / maxHp;
        if (hp <= 0)
        {
            anim.enabled = false;
            StartCoroutine(destroy());
            //     enemyManager.enemieList.Remove(gameObject);
        }
        
    }
    IEnumerator destroy()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
