﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class HumanBone
{   
   public HumanBodyBones bone;
}
public class WeaponIK : MonoBehaviour
{
    [SerializeField]
    Transform targetTransform;
    [SerializeField]
    Transform aimTransform;
    [SerializeField]
    Transform[] bone;
    [SerializeField]
    int interation = 10;
    [Range(0, 1)]
    public float weight = 1f;
    
    Transform[] boneTransform;
    Camera mainCamera;
    [SerializeField]
    float angleLimit = 90f;
    [SerializeField]
    float distanceLimit = 1.5f;
    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    Transform muzzleTransform;
    [SerializeField]
    GameObject Muzzle;
    float recoilMaxRotation = 45f;
    [SerializeField]
    Transform rightLowerArm;
    [SerializeField]
    Transform leftLowerArm;
    [SerializeField] LayerMask mouseAimMask;
    private float recoilTimer;
    [SerializeField]
    AnimationCurve recoilCurve;
    [SerializeField]
    float recoilDuration = 0.25f;
    bool canShoot;
    [SerializeField]
    float fireDelay;
    public bool is2Handes;
    void Start()
    {
        // rb = GetComponent<Rigidbody>();
        //playerMovement = GetComponent<PlayerMovement>();
        mainCamera = Camera.main;
        canShoot = true;
    }
    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && canShoot )
        {
           
            canShoot = false;
           StartCoroutine (Fire());
        }
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mouseAimMask))
        {
            targetTransform.position = hit.point;
        }
    }
    IEnumerator Fire()
    {
        
        yield return new WaitForSeconds(fireDelay);
        recoilTimer = Time.time;
    
        GameObject muz = Instantiate(Muzzle, muzzleTransform.position, muzzleTransform.rotation);
        Destroy(muz, 0.2f);
        GameObject bullet=  Instantiate(bulletPrefab, muzzleTransform.position, muzzleTransform.rotation);
        canShoot = true;
    }
    Vector3 GetTargetPosition()
    {
        Vector3 targetDirection = targetTransform.position - aimTransform.position;
        Vector3 aimDirection = aimTransform.forward;
        float blendOut = 0f;
        float targetAngle = Vector3.Angle(targetDirection, aimDirection);
        if (targetAngle > angleLimit)
        {
            blendOut += (targetAngle - angleLimit) / 50f;
        }
        float targetDistance = targetDirection.magnitude;
        if (targetDistance < distanceLimit)
        {
            blendOut += distanceLimit - targetDistance;
        }
        Vector3 direction = Vector3.Slerp(targetDirection, aimDirection, blendOut);
        return aimTransform.position + direction;
    }
    void LateUpdate()
    {
      //  if (rb.velocity == Vector3.zero)
      //  {
            Vector3 targetPosition = GetTargetPosition();
            for (int i = 0; i < bone.Length; i++)
            {

                AimAtTarget(bone[i], targetPosition, weight);
            }
       // }
        if (recoilTimer < 0)
        {
            return;
        }

        float curveTime = (Time.time - recoilTimer) / recoilDuration;
        if (curveTime > 1f)
        {
            recoilTimer = -1;
        }
        else
        {           
                rightLowerArm.Rotate(-Vector3.down, recoilCurve.Evaluate(curveTime) * recoilMaxRotation, Space.Self);          
        }
    }

    void AimAtTarget(Transform bone,Vector3 targetPosition,float weight)
    {
        Vector3 aimDirection = aimTransform.forward;
        Vector3 targetDirection = targetPosition - aimTransform.position;
        Quaternion aimToward = Quaternion.FromToRotation(aimDirection,targetDirection);
        Quaternion blendedRotation = Quaternion.Slerp(Quaternion.identity, aimToward, weight);
        bone.rotation = blendedRotation * bone.rotation;
    }
}
