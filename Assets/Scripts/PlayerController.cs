using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{

    Vector3 target;
    [SerializeField] Transform[] wayPoints;
    [SerializeField]private int waypointIndex=0;
    public bool canMove;
    private NavMeshAgent agent;
    Animator anim;
    Rigidbody rb;
    public float distance;
    [SerializeField] GameObject cam1;
    [SerializeField] GameObject cam2;
    WeaponIK ik;
    public bool isStop;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        ik = GetComponent<WeaponIK>();
        canMove = false;
        UpdateDestination();
        cam2.SetActive(false);
        isStop = true;
    }

   
    void Update()
    {
        distance = Vector3.Distance(transform.position, target);

        if (Vector3.Distance(transform.position, target) < 1 && canMove)
        {
            UpdateWaypointIndex();
            UpdateDestination();
        }
        if(isStop)
        {
            canMove = false;
            cam1.SetActive(false);
            cam2.SetActive(true);
            anim.SetBool("run", false);
            ik.enabled = true;
        }else
        {
            cam1.SetActive(true);
            cam2.SetActive(false);
            anim.SetBool("run", true);
            ik.enabled = false;
            canMove = true;
        }      
        if (Input.GetButtonDown("Jump"))
        {
            canMove = true;
        }
        
    }
    void UpdateDestination()
    {
        target = wayPoints[waypointIndex].position;
        agent.SetDestination(target);       
        cam1.SetActive(true);
        cam2.SetActive(false);
    }
    void UpdateWaypointIndex()
    {
        waypointIndex++;
        if (waypointIndex == wayPoints.Length)
        {
            waypointIndex = 0;
        }
        //if (agent.path.status == NavMeshPathStatus.PathComplete)
        //{
        //    canMove = false;
        //}
    }
    
}
