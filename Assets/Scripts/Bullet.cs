﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float speed;
    Vector3 lastPos;
    float strange;
    //[SerializeField]
    //GameObject decal;
    [SerializeField]
    int damage;
    Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 3);
        //lastPos = transform.position;
    }

    public void Update()
    {    
        rb.velocity = transform.TransformDirection(Vector3.forward * speed);
    }
    private void FixedUpdate()
    {
        //RaycastHit hit;
        //if (Physics.Linecast(lastPos, transform.position, out hit))
        //{
        //    //if (hit.transform.GetComponent<EnemyHealth>() != null)
        //    //{
        //    //    hit.transform.GetComponent<EnemyHealth>().Damage(damage);
        //    //}
        //    // 
        //    //GameObject d = Instantiate(decal);
        //    //d.transform.position = hit.point + hit.normal * 0.01f;
        //    //d.transform.rotation = Quaternion.LookRotation(-hit.normal);
        //    //Destroy(d, 5);
        //    //   
        //    Destroy(gameObject);
        //}
        //lastPos = transform.position;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemie")
        {
            collision.gameObject.GetComponent<EnemieScript>().MakeDamage(damage);
        }
        Destroy(gameObject);
    }
}

