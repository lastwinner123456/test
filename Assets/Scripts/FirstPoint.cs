using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPoint : MonoBehaviour
{
    GameObject player;
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        player.gameObject.GetComponent<PlayerController>().isStop = true;
        player.GetComponent<PlayerController>().GetComponent<WeaponIK>().enabled = false;
        if (Input.GetMouseButton(0))
        {
            player.GetComponent<PlayerController>().isStop = false;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().isStop = true;
        }
    }
}
